local cmd = vim.cmd
local fn = vim.fn
local g = vim.g
local opt = vim.opt

--- Indentation options
opt.expandtab = true
opt.shiftwidth = 4
opt.smartindent = true
opt.softtabstop = 4
opt.tabstop = 4

--- Search options
opt.ignorecase = true
opt.smartcase = true

--- Performance options
opt.lazyredraw = true
opt.updatetime = 100

--- User Interface options
opt.cursorline = true
opt.number = true
opt.signcolumn = 'yes'
opt.title = true
opt.visualbell = true

--- Advanced options
opt.completeopt = {'menuone', 'noinsert', 'noselect'}
opt.fillchars:append{vert = ' '}
opt.guifont = 'FiraCode Nerd Font'
opt.helplang = {'ja', 'en'}
opt.listchars = {eol = '', space = '·', tab = ' '}
opt.matchpairs:append{'（:）', '［:］', '｛:｝', '〈:〉', '《:》', '「:」', '『:』', '【:】', '〔:〕', '“:”', '‘:’'}
opt.path:append'**'
opt.scroll = 3
opt.shortmess:append'c'
opt.showmatch = true
opt.spelllang = {'en', 'cjk'}
opt.termguicolors = true
-- opt.timeoutlen = 500

--- Status Lines
opt.statusline='%0*%< '
opt.statusline:append'%2*%{mode() == "n" ? " " : ""}'
opt.statusline:append'%4*%{mode() == "i" ? " " : ""}'
opt.statusline:append'%5*%{mode() == "v" ? " " : ""}'
opt.statusline:append'%5*%{mode() == "" ? " " : ""}'
-- opt.statusline:append'%3*%{mode() == "s" ? " " : ""}'
opt.statusline:append'%1*%{mode() == "r" ? " " : ""}'
opt.statusline:append'%2*%{mode() == "c" ? " " : ""}'
opt.statusline:append'%4*%{mode() == "!" ? " " : ""}'
opt.statusline:append'%7*%{mode() == "t" ? " " : ""}'
opt.statusline:append'%0*%f %h%w%m%r%=%l:%v '

if fn.executable'rg' == 1 then
  opt.grepprg = 'rg --vimgrep $*'
  opt.grepformat = '%f:%l:%c:%m'
end

--- Variables
g.gitgutter_sign_modified = '!'
g.mapleader = ' '
g.user_emmet_install_global = 0
g.user_emmet_leader_key = ','

--- Commands
cmd'autocmd BufWritePost * if getline(1) =~ "^#!.*/bin/" | silent !chmod +x <afile> | endif'
cmd'autocmd TextYankPost * silent! lua vim.highlight.on_yank()'
cmd'colorscheme ry-dark'

-- cmd[[
-- augroup numbertoggle
-- autocmd!
-- autocmd BufLeave,FocusLost,InsertEnter,WinLeave * set statusline=%<\ \ \ %f\ %h%w%m%r%=%l:%v\ 
-- augroup END
-- ]]

-- cmd[[
-- augroup numbertoggle
-- autocmd!
-- autocmd BufEnter,FocusGained,InsertLeave,WinEnter * if &number && mode() != "i" | set relativenumber | endif
-- autocmd BufLeave,FocusLost,InsertEnter,WinLeave * if &number | set norelativenumber | endif
-- augroup END
-- ]]
