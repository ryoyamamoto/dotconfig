local cmp = require'cmp'
local lspkind = require'lspkind'
local luasnip = require'luasnip'

local has_words_before = function ()
  local line, col = unpack(vim.api.nvim_win_get_cursor(0))
  return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match'%s' == nil
end

local call = function (function_name, option)
  local option = option or {}
  local modes = option['modes']
  option['modes'] = nil
  return cmp.mapping(cmp.mapping[function_name](option), modes)
end

local confirm = function (...)
  return call('confirm', ...)
end

local select_next_item = function (...)
  return call('select_next_item', ...)
end

local select_prev_item = function (...)
  return call('select_prev_item', ...)
end

local customize = function (option)
  local action = option['action']
  local modes = option['modes']
  return cmp.mapping(action, modes)
end

cmp.setup{
  formatting = {
    format = lspkind.cmp_format{
      with_text = false,
      menu = {
        buffer = '[Buffer]',
        cmdline = '[Cmd]',
        luasnip = '[LuaSnip]',
        nvim_lsp = '[LSP]',
        path = '[Path]',
      },
    },
  },
  mapping = {
    ['<Tab>'] = customize{
      action = function(fallback)
        if cmp.visible() then
          cmp.confirm{select = true}
        elseif luasnip.jumpable(1) then
          luasnip.jump(1)
        elseif has_words_before() then
          cmp.complete()
        else
          fallback()
        end
      end,
      modes = {'c', 'i', 's'},
    },
    ['<S-Tab>'] = customize{
      action = function(fallback)
        if luasnip.jumpable(-1) then
          luasnip.jump(-1)
        else
          fallback()
        end
      end,
      modes = {'i', 's'},
    },
    ['<C-n>'] = select_next_item{
      behavior = cmp.SelectBehavior.Select,
      modes = {'c', 'i'},
    },
    ['<C-p>'] = select_prev_item{
      behavior = cmp.SelectBehavior.Select,
      modes = {'c', 'i'},
    },
    ['<C-e>'] = customize{
      action = function(fallback)
        if cmp.visible() then
          cmp.abort()
        else
          fallback()
        end
      end,
      modes = {'c', 'i'}
    },
    ['<CR>'] = confirm{select = false},
    ['<Esc>'] = customize{
      action = function(fallback)
        if cmp.visible() then
          cmp.abort()
        else
          fallback()
        end
      end,
      modes = {'c', 'i'},
    },
  },
  snippet = {
    expand = function(args)
      luasnip.lsp_expand(args.body)
    end,
  },
  sources = cmp.config.sources{
    {name = 'luasnip'},
    {name = 'nvim_lsp'},
    {name = 'buffer'},
  },
}

cmp.setup.cmdline(':', {
  sources = cmp.config.sources{
    {name = 'path'},
    {name = 'cmdline'},
  },
})

cmp.setup.cmdline('/', {
  sources = cmp.config.sources{
    {name = 'buffer'},
  },
})

require'luasnip/loaders/from_vscode'.lazy_load()
