local function on_attach (client, buffer)
  local function nnoremap (lhs, rhs)
    vim.api.nvim_buf_set_keymap(buffer, 'n', lhs, rhs, {noremap = true})
  end

  nnoremap('gD', '<Cmd>lua vim.lsp.buf.declaration()<CR>')
  -- nnoremap('gd', '<Cmd>lua vim.lsp.buf.definition()<CR>')
  nnoremap('gd', '<Cmd>lua require"telescope.builtin".lsp_definitions()<CR>')
  nnoremap('K', '<Cmd>lua vim.lsp.buf.hover()<CR>')
  -- nnoremap('gi', '<Cmd>lua vim.lsp.buf.implementation()<CR>')
  nnoremap('gi', '<Cmd>lua require"telescope.builtin".lsp_implementations()<CR>')
  nnoremap('<C-K>', '<Cmd>lua vim.lsp.buf.signature_help()<CR>')
  -- nnoremap('<Space>wa', '<Cmd>lua vim.lsp.buf.add_workspace_folder()<CR>')
  -- nnoremap('<Space>wr', '<Cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>')
  -- nnoremap('<Space>wl', '<Cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>')
  nnoremap('<Space>D', '<Cmd>lua vim.lsp.buf.type_definition()<CR>')
  nnoremap('<Space>rn', '<Cmd>lua vim.lsp.buf.rename()<CR>')
  -- nnoremap('<Space>ca', '<Cmd>lua vim.lsp.buf.code_action()<CR>')
  nnoremap('<Leader>ca', '<Cmd>lua require"telescope.builtin".lsp_code_action()<CR>')
  -- nnoremap('gr', '<Cmd>lua vim.lsp.buf.references()<CR>')
  nnoremap('gr', '<Cmd>lua require"telescope.builtin".lsp_references()<CR>')
  -- nnoremap('<Space>e', '<Cmd>lua vim.lsp.diagnostic.show_line_diagnostics()<CR>')
  nnoremap('<Space>e', '<Cmd>lua vim.lsp.diagnostic.show_line_diagnostics()<CR>')
  -- nnoremap('[d', '<Cmd>lua vim.lsp.diagnostic.goto_prev()<CR>')
  -- nnoremap(']d', '<Cmd>lua vim.lsp.diagnostic.goto_next()<CR>')
  -- nnoremap('<Space>q', '<Cmd>lua vim.lsp.diagnostic.set_loclist()<CR>')
  -- nnoremap('<Space>f', '<Cmd>lua vim.lsp.buf.formatting()<CR>')

  if client.resolved_capabilities.document_formatting then
    nnoremap('g=', '<Cmd>lua vim.lsp.buf.formatting()<CR>')
  elseif client.resolved_capabilities.document_range_formatting then
    nnoremap('g=', '<Cmd>lua vim.lsp.buf.range_formatting()<CR>')
  end
end

local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require'cmp_nvim_lsp'.update_capabilities(capabilities)

local handlers = {
  ["textDocument/publishDiagnostics"] = vim.lsp.with(
    vim.lsp.diagnostic.on_publish_diagnostics, {
      virtual_text = true,
      signs = true,
      underline = true,
      update_in_insert = false,
      -- update_in_insert = true,
      -- severity_sort = true,
    }
  ),
}

local servers = {'gopls', 'tsserver'}
local nvim_lsp = require'lspconfig'
for _, lsp in ipairs(servers) do
  nvim_lsp[lsp].setup{
    handlers = handlers,
    on_attach = on_attach,
    capabilities = capabilities,
  }
end

-- Rust
local opts = {
  tools = {
    hover_actions = {
      border = 'none',
    },
  },

  server = {
    handlers = handlers,
    on_attach = on_attach,
    capabilities = capabilities,
    settings = {
      ['rust-analyzer'] = {
        cargo = {
          loadOutDirsFromCheck = true
        },
        checkOnSave = {
          command = 'clippy'
        },
        procMacro = {
          enable = true
        },
      },
    },
  },
}

require'rust-tools'.setup(opts)
