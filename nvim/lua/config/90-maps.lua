local function _map (mode, lhs, rhs, opts)
  opts = opts or {}
  vim.api.nvim_set_keymap(mode, lhs, rhs, opts)
end

local function _noremap (mode, lhs, rhs, opts)
  opts = opts or {}
  opts.noremap = true
  vim.api.nvim_set_keymap(mode, lhs, rhs, opts)
end

local function t (str)
  return vim.api.nvim_replace_termcodes(str, true, true, true)
end

local function map (...)
  _map('', ...)
end

local function noremap (...)
  _noremap('n', ...)
end

local function nmap (...)
  _map('n', ...)
end

local function nnoremap (...)
  _noremap('n', ...)
end

local function xmap (...)
  _map('x', ...)
end

local function xnoremap (...)
  _noremap('x', ...)
end

local function inoremap (...)
  _noremap('i', ...)
end

local function cnoremap (...)
  _noremap('c', ...)
end

function _G.preserve (cmd)
  local lazyredraw = vim.opt.lazyredraw
  local view = vim.fn.winsaveview()
  vim.opt.lazyredraw = true
  vim.cmd(cmd)
  vim.fn.winrestview(view)
  vim.opt.lazyredraw = lazyredraw
end

function _G.smart_esc ()
  return vim.fn.pumvisible() == 1 and t'<C-E>' or t'<Esc>'
end

function _G.smart_tab ()
  return vim.fn.pumvisible() == 1 and t'<C-N>' or t'<Tab>'
end

function _G.smart_s_tab ()
  return vim.fn.pumvisible() == 1 and t'<C-P>' or t'<S-Tab>'
end

--- Normal/Visual/Select/Operator-pending mode
-- map('<Leader>e', '<Plug>(easymotion-bd-e)')
-- map('<Leader>E', '<Plug>(easymotion-bd-E)')
-- map('<Leader>l', '<Plug>(easymotion-bd-jk)')
-- map('<Leader>n', '<Plug>(easymotion-bd-n)')
-- -- map('<Leader>c', '<Plug>(easymotion-bd-f)')
map('<Leader>r', '<Cmd>registers<CR>')
-- map('<Leader>s', '<Plug>(easymotion-bd-f)')
-- map('<Leader>w', '<Plug>(easymotion-bd-w)')
-- map('<Leader>W', '<Plug>(easymotion-bd-W)')
noremap('gh', '0')
noremap('gl', '$')
noremap('gs', '^')

--- Normal mode
nmap('<C-_>', '<Plug>CommentaryLine')
-- nmap('<Leader>l', '<Plug>(easymotion-overwin-line)')
-- nmap('<Leader>c', '<Plug>(easymotion-overwin-f)')
-- nmap('<Leader>s', '<Plug>(easymotion-overwin-f)')
-- nmap('<Leader>w', '<Plug>(easymotion-overwin-w)')

-- nmap('<Leader>a', '<Plug>Anywhere')

nnoremap('g;', '@:')
nnoremap('g=', '<Cmd>lua preserve"normal gg=G"<CR>')
nnoremap('s', 'xi')
nnoremap('Y', 'y$')
nnoremap('ZS', ':saveas ')
nnoremap('ZU', '<Cmd>update<CR>')
nnoremap('&', '<Cmd>&&<CR>')
nnoremap('<C-L>', '<Cmd>nohlsearch<CR><C-L>')
nnoremap('<Leader>', '<Nop>')
nnoremap('<Leader>bb', '<Cmd>lua require"telescope.builtin".buffers()<CR>')
nnoremap('<Leader>bd', '<Cmd>bdelete<CR>')
nnoremap('<Leader>bk', '<Cmd>bdelete<CR>')
nnoremap('<Leader>bn', '<Cmd>bnext<CR>')
nnoremap('<Leader>bN', '<Cmd>enew<CR>')
nnoremap('<Leader>bp', '<Cmd>bprevious<CR>')
nnoremap('<Leader>br', '<Cmd>edit!<CR>')
nnoremap('<Leader>D', '"*D')
nnoremap('<Leader>d', '"*d')
nnoremap('<leader>fb', '<Cmd>lua require"telescope.builtin".buffers()<CR>')
nnoremap('<leader>fc', '<Cmd>lua require"telescope.builtin".commands()<CR>')
nnoremap('<leader>fd', '<Cmd>Telescope diagnostics<CR>')
-- nnoremap('<Leader>fe', '<Cmd>Explore<CR>')
nnoremap('<leader>fb', '<Cmd>lua require"telescope".extensions.file_browser.file_browser()<CR>')
nnoremap('<leader>ff', '<Cmd>lua require"telescope.builtin".find_files()<CR>')
nnoremap('<leader>fg', '<Cmd>lua require"telescope.builtin".live_grep()<CR>')
nnoremap('<leader>f*', '<Cmd>lua require"telescope.builtin".grep_string()<CR>')
nnoremap('<Leader>fh', '<Cmd>lua require"telescope.builtin".help_tags()<CR>')
nnoremap('<Leader>gg', '<Cmd>lua require"telescope.builtin".git_files()<CR>')
nnoremap('<Leader>gs', '<Cmd>lua require"telescope.builtin".git_status()<CR>')
nnoremap('<Leader>m', '`mzz')
nnoremap('<Leader>P', '"*P')
nnoremap('<Leader>p', '"*p')
nnoremap('<Leader>ss', '<Cmd>lua require"telescope.builtin".lsp_document_symbols()<CR>')
nnoremap('<Leader>sw', '<Cmd>lua require"telescope.builtin".lsp_workspace_symbols()<CR>')
nnoremap('<Leader>sd', '<Cmd>lua require"telescope.builtin".lsp_dynamic_workspace_symbols()<CR>')
nnoremap('<Leader>tc', '<Cmd>tabclose<CR>')
nnoremap('<Leader>te', '<Cmd>tabedit<CR>')
nnoremap('<Leader>tf', '<Cmd>tabfirst<CR>')
nnoremap('<Leader>tl', '<Cmd>tablast<CR>')
nnoremap('<Leader>tn', '<Cmd>tabnext<CR>')
nnoremap('<Leader>to', '<Cmd>tabonly<CR>')
nnoremap('<Leader>tp', '<Cmd>tabprevious<CR>')
nnoremap('<Leader>tt', '<Cmd>tabs<CR>')
nnoremap('<Leader>Y', '"*y$')
nnoremap('<Leader>y', '"*y`]')

--- Visual mode
-- xmap('v', '<Plug>(expand_region_expand)')
-- xmap('<S-V>', '<Plug>(expand_region_shrink)')
xmap('<C-_>', '<Plug>Commentary')
xnoremap('y', 'y`]')
xnoremap('&', '<Cmd>&&<CR>')
xnoremap('<Leader>', '<Nop>')
xnoremap('<Leader>d', '"*d')
xnoremap('<Leader>p', '"*p')
xnoremap('<Leader>y', '"*y`]')

--- Insert mode
inoremap('jk', 'v:lua.smart_esc()', {expr = true})
-- inoremap('<C-E>', '<Plug>luasnip-next-choice')
inoremap('<Esc>', 'v:lua.smart_esc()', {expr = true})
inoremap('<Tab>', 'v:lua.smart_tab()', {expr = true})
inoremap('<S-Tab>', 'v:lua.smart_s_tab()', {expr = true})
-- inoremap('<Tab>', 'v:lua.tab_complete()', {expr = true}))
-- inoremap('<S-Tab>', 'v:lua.s_tab_complete()', {expr = true})

--- Command-line mode
cnoremap('%%', 'getcmdtype() == ":" ? expand("%:h")."/" : "%%"', {expr = true})
