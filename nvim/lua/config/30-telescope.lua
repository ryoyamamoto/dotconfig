require'telescope'.setup{
  defaults = {
    sorting_strategy = 'ascending',
    layout_strategy = 'flex',
    layout_config = {
      horizontal = {
        preview_width = 0.5,
        prompt_position = 'top',
      },
      vertical = {
        mirror = true,
        preview_height = 0.5,
        prompt_position = 'top',
      }
    },
    prompt_prefix = ' ',
    selection_caret = ' > ',
    entry_prefix = '   ',
    borderchars = {'─', '│', '─', '│', '┌', '┐', '┘', '└'},
    get_status_text = function (self)
      local xx = (self.stats.processed or 0) - (self.stats.filtered or 0)
      local yy = self.stats.processed or 0
      if xx == 0 and yy == 0 then
        return ''
      end
      return string.format('%s/%s ', xx, yy)
    end,
    -- file_ignore_patterns = {'^.git/', '^node_modules/', '^target/', '%.zwc'},
  },
  pickers = {
    find_files = {
      -- hidden = true,
      -- no_ignore = true,
      -- follow = true,
    },
    live_grep = {
      layout_strategy = 'vertical',
    },
    lsp_document_symbols = {
      layout_strategy = 'vertical',
    },
    lsp_workspace_diagnostics = {
      layout_strategy = 'vertical',
    },
  },
}

require'telescope'.load_extension'file_browser'
require'telescope'.load_extension'fzf'
