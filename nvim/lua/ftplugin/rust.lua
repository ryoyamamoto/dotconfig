local ls = require'luasnip'
local s = ls.snippet
local node = ls.snippet_node
local t = ls.text_node
local i = ls.insert_node
local func = ls.function_node
local choice = ls.choice_node
local dynamicn = ls.dynamic_node

ls.snippets = {
  rust = {
    s('ref', {t'&', i(1, 'expr')}),
    s('refm', {t'&mut ', i(1, 'expr')}),
    s('let', {t'let ', i(1), t' = ', i(2, 'expr')}),
    s('letm', {t'let mut ', i(1), t' = ', i(2, 'expr')}),
    s('not', {t'!', i(1, 'expr')}),
    s('print', {t'print!(', i(1), t')'}),
    s('println', {t'println!(', i(1), t')'}),
    s('arm', {i(1, 'Some(x)'), t' => ', i(2, 'expr'), t','}),
    s('method', {
      t'fn ', i(1, 'name'), t'(&self', i(2), t') ', i(3, '-> RetType '), t'{',
      t{'', '\t'}, i(4, 'unimplemented!();'),
      t{'', '}'}
    }),
    s('struct', {
      t'struct ', i(1, 'Name'), t' {',
      t{'', '\t'}, i(2, 'field'), t': ', i(3, 'Type'), t',',
      t{'', '}'}
    }),
    s('field', {i(1, 'name'), t': ', i(2, 'Type')}),
    s('param', {i(1, 'name'), t': ', i(2, 'Type')}),
  },
}

local opt = vim.opt
local b = vim.b

opt.formatoptions:remove{'o'}
opt.textwidth = 80
b.rustfmt_autosave = 1
