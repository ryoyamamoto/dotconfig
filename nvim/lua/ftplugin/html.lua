local cmd = vim.cmd
local b = vim.b
local opt_local = vim.opt_local

opt_local.matchpairs:append'<:>'

b.html_indent_autotags = 'html'

cmd'EmmetInstall'
