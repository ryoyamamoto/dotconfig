local bootstrap = {}

local PKGS = {
  "savq/paq-nvim";
  'nvim-lua/plenary.nvim';
  -- treesitter
  {'nvim-treesitter/nvim-treesitter', run = function () vim.cmd'TSInstallSync all' end};
  'nvim-treesitter/nvim-treesitter-textobjects';
  -- lsp
  'neovim/nvim-lspconfig';
  'onsails/lspkind-nvim';
  -- completion
  'hrsh7th/cmp-buffer';
  'hrsh7th/cmp-cmdline';
  'hrsh7th/cmp-nvim-lsp';
  'hrsh7th/cmp-path';
  'hrsh7th/nvim-cmp';
  -- snippets
 'L3MON4D3/LuaSnip';
  'rafamadriz/friendly-snippets';
  'saadparwaiz1/cmp_luasnip';
  -- gitsigns
  'lewis6991/gitsigns.nvim';
  -- telescope
  'nvim-telescope/telescope.nvim';
  'nvim-telescope/telescope-file-browser.nvim';
  {'nvim-telescope/telescope-fzf-native.nvim', run = 'make'};
  'kyazdani42/nvim-web-devicons';
  -- emmet
  'mattn/emmet-vim';
  -- rust
  'rust-lang/rust.vim';
  'simrat39/rust-tools.nvim';
  -- misc.
  {'nvim-orgmode/orgmode', run = function () require'orgmode'.setup{} end};
  'tpope/vim-abolish';
  'tpope/vim-commentary';
  'tpope/vim-repeat';
  'tpope/vim-rsi';
  'tpope/vim-surround';
  'tpope/vim-unimpaired';
  'windwp/nvim-autopairs';
  'vim-jp/vimdoc-ja';
}

local function clone_paq ()
  local path = vim.fn.stdpath'data' .. '/site/pack/paqs/start/paq-nvim'
  if vim.fn.empty(vim.fn.glob(path)) > 0 then
    vim.fn.system{
      'git',
      'clone',
      '--depth=1',
      'https://github.com/savq/paq-nvim.git',
      path
    }
  end
end

function bootstrap.paq ()
  clone_paq()

  -- Load Paq
  vim.cmd'packadd paq-nvim'
  local paq = require'paq'

  -- Exit nvim after installing plugins
  vim.cmd'autocmd User PaqDoneInstall quit'

  --Read and install packages
  paq(PKGS)
  paq.install()
end

return bootstrap
