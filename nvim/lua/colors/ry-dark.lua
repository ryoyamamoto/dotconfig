--- ry-dark.lua

local none = "NONE"
local bg = "bg"
local fg = "fg"
local bold = "bold"
local underline = "underline"
local italic = "italic"

local colors = {
  term = none, cterm = none, ctermfg = none, ctermbg = none,
  gui = none, guifg = none, guibg = none, guisp = none,
}

local function setcolors (cmd, group, args)
  local args = vim.tbl_extend("force", colors, args)
  local cmd = string.format("%s %s", cmd, group)
  for key, value in pairs(args) do
    cmd = string.format("%s %s=%s", cmd, key, value)
  end
  vim.cmd(cmd)
end

local function colorscheme (...)
  setcolors ("autocmd ColorScheme * highlight", ...)
end

local function highlight (...)
  setcolors ("highlight", ...)
end

local function highlight_link (from, to)
  local cmd = string.format("highlight link %s %s", from, to)
  vim.cmd(cmd)
end

local function highlight_clear ()
  vim.cmd("highlight clear")
end


local function syntax_reset ()
  vim.cmd("syntax reset")
end

local sign_define = vim.fn.sign_define

highlight_clear()
syntax_reset()
vim.g.color_name = "ry-dark"

---
-- Syntax colors
--
local defaultfg = "#d6d6d6" -- Foreground (gy-8.5)
local defaultbg = "#000000" -- Background (black)

local red = "#f19896"        -- Statement, Conditional, Repeat, Label, Operator, Exception, PreCondit, Error, etc. (lt+2)
local green = "#c7d36d"      -- String (lt+10)
local yellow = "#f2d96e"     -- Constant, Charcter, Number, Boolean, Float, Type, Structure, Typedef (lt+8)
local blue = "#6c9ac5"       -- Function, Method (lt+18)
local magenta = "#b088b5"    -- Keyword, PreProc, StorageClass (lt+22)
local cyan = "#5bafc4"       -- Special, TSStringRegex (lt+16)
local orange = "#ffbe71"     -- Type, Todo, TSVariableBuiltin (lt+6)

local dark_brown = "#806c5c" -- Comment (g6)

---
-- Standard syntax highlighting
--
highlight("Comment", {gui = italic, guifg = dark_brown})
highlight("Constant", {guifg = yellow})
highlight("String", {guifg = green}) -- default: links to Constant
-- highlight_link("Character", "Constant")
-- highlight_link("Number", "Constant")
-- highlight_link("Boolean", "Constant")
-- highlight_link("Float", "Constant")
highlight("Identifier", {guifg = defaultfg})
highlight("Function", {guifg = blue}) -- default: links to Identifier
highlight("Statement", {guifg = red}) -- 命令文
-- highlight_link("Conditional", "Statement") -- if, then, else, endif, switch, その他
-- highlight_link("Repeat", "Statement")      -- for, do, while, その他
-- highlight_link("Label", "Statement")       -- case, default, その他
-- highlight_link("Operator", "Statement")    -- "sizeof", "+", "*", その他
highlight("Operator",{guifg = cyan})        -- "sizeof", "+", "*", その他
-- highlight("Keyword", {guifg = magenta})     -- その他のキーワード
-- highlight_link("Exception", "Statement")   -- try, catch, throw
highlight("PreProc", {guifg = magenta})     -- 一般的なプリプロセッサー命令
-- highlight_link("Include", "PreProc")       -- #include プリプロセッサー
-- highlight_link("Define", "PreProc")        -- #define プリプロセッサー
-- highlight_link("Macro", "PreProc")        -- Defineと同値
highlight("PreCondit", {guifg = red}) -- プリプロセッサーの #if, #else #endif, その他

highlight("Type", {guifg = orange}) -- int, long, char, その他
highlight("StorageClass", {guifg = magenta}) -- static, register, volatile, その他
-- highlight("Structure", {guifg = orange}) -- struct, union, enum, その他
-- highlight("Typedef", {guifg = magenta}) -- typedef宣言

highlight("Special", {guifg = cyan}) -- 特殊なシンボル
-- highlight("SpecialChar", {guifg = cyan}) -- 特殊な文字定数
-- highlight("Tag", {guifg = cyan}) -- この上で CTRL-] を使うことができる
highlight("Delimiter", {guifg = dark_brown}) -- 注意が必要な文字
-- highlight("SpecialComment", {guifg = cyan}) -- コメント内の特記事項
highlight("Debug", {guifg = dark_brown}) -- デバッグ命令

highlight("Underlined", {gui = underline, guifg = blue}) -- 目立つ文章, HTMLリンク

-- highlight("Ignore", {}) -- (見た目上)空白, 不可視, hl-Ignore

highlight("Error", {guifg = red}) -- エラーなど, なんらかの誤った構造

highlight("Todo", {guifg = orange}) -- 特別な注意が必要なもの; 大抵は TODO FIXME XXX などのキーワード

---
-- Tree-sitter
--
highlight("TSNone", {guifg = none}) -- 

-- highlight default link TSPunctDelimiter Delimiter
-- highlight default link TSPunctBracket Delimiter
-- highlight default link TSPunctSpecial Delimiter

-- highlight default link TSConstant Constant
highlight_link("TSConstBuiltin", "TSConstant") -- default: links to Special
-- highlight default link TSConstMacro Define
-- highlight default link TSString String
highlight("TSStringRegex", {guifg = cyan}) -- default: links to String
-- highlight default link TSStringEscape SpecialChar
-- highlight default link TSStringSpecial SpecialChar
-- highlight default link TSCharacter Character
-- highlight default link TSNumber Number
-- highlight default link TSBoolean Boolean
-- highlight default link TSFloat Float

-- highlight default link TSFunction Function
highlight_link("TSFuncBuiltin", "TSFunction") -- default: links to Special
-- highlight default link TSFuncMacro Macro
-- highlight_link("TSParameter", "Identifier")
-- highlight default link TSParameterReference TSParameter
-- highlight default link TSMethod Function
-- highlight_link("TSField", "Identifier")
-- highlight_link("TSProperty", "Identifier")
-- highlight default link TSProperty Identifier
highlight_link("TSConstructor", "TSType") -- default: links to Special
highlight_link("TSAnnotation", "TSType") -- default: links to Preproc
-- highlight default link TSAttribute PreProc
-- highlight default link TSNamespace Include
-- highlight("TSSymbol", {guifg = yellow}) -- 

-- highlight default link TSConditional Conditional
-- highlight default link TSRepeat Repeat
-- highlight default link TSLabel Label
-- highlight default link TSOperator Operator
-- highlight("TSKeyword", {guifg = magenta}) -- default: links to Keyword
highlight("TSKeywordFunction", {guifg = magenta}) -- default: links to Keyword
-- Unary and binary operators that are English words: `and`, `or` in Python; `sizeof` in C
-- highlight("TSKeywordOperator", {guifg = red}) -- default links to TSOperator
highlight_link("TSKeywordReturn", "TSConditional") -- default: links to TSKeyword
-- highlight default link TSException Exception

-- highlight default link TSType Type
-- highlight default link TSTypeBuiltin Type
-- highlight default link TSInclude Include

-- Variable names defined by the language: `this` or `self` in JavaScript
highlight("TSVariableBuiltin", {guifg = orange}) -- default: links to Special

-- highlight default link TSText TSNone
-- highlight default TSStrong term=bold cterm=bold gui=bold
-- highlight default TSEmphasis term=italic cterm=italic gui=italic
-- highlight default TSUnderline term=underline cterm=underline gui=underline
-- highlight default TSStrike term=strikethrough cterm=strikethrough gui=strikethrough
-- highlight("TSMath", {guifg = cyan}) -- default: links to Special
-- highlight default link TSTextReference Constant
-- highlight default link TSEnvironment Macro
-- highlight default link TSEnvironmentName Type
-- highlight default link TSTitle Title
-- highlight default link TSLiteral String
-- highlight default link TSURI Underlined

-- highlight default link TSComment Comment
highlight("TSNote", {guifg = blue}) -- default: links to SpecialComment
highlight("TSWarning", {guifg = orange}) -- default: links to Todo
highlight("TSDanger", {guifg = red}) -- default: links to WarningMsg

highlight("TSTag", {guifg = magenta}) -- Tags like HTML tag names
highlight("TSTagDelimiter", {guifg = magenta}) -- Tag delimiters like `<` `>` `/`
highlight("TSTagAttribute", {guifg = blue}) -- HTML tag attributes

---
-- UI colors
--
local white = "#ffffff"  -- IncSearch, Search
local grey78 = "#c6c6c6"
local grey70 = "#b2b2b2"
local grey62 = "#9e9e9e"
local grey50 = "#808080"
local grey39 = "#626262"
local grey30 = "#4e4e4e"
local grey23 = "#3a3a3a"
local grey15 = "#262626"
local grey7 = "#121212"  -- ColorColumn, CursorColumn, CursorLine
local black = "#000000"

local errfg = "#ef6c70"  -- b2
local errbg = "#3e2c30"  -- dkg2
local warnfg = "#ffad36" -- b6
local warnbg = "#443e30" -- dkg8
-- local infofg = "#2981c0" -- b18
local infofg = "#6c9ac5" -- lt+18
local infobg = "#222933" -- dkg18
-- local hintfg = "#c6dde2" -- p16
local hintfg = "#add1da" -- p+16
local hintbg = "#273439" -- dkg16

---
-- LSP Diagnostic colors
--
sign_define("DiagnosticSignError", {text = "", texthl = "DiagnosticSignError", linehl = "", numhl = ""})
sign_define("DiagnosticSignWarn", {text = "", texthl = "DiagnosticSignWarn", linehl = "", numhl = ""})
sign_define("DiagnosticSignInfo", {text = "", texthl = "DiagnosticSignInfo", linehl = "", numhl = ""})
sign_define("DiagnosticSignHint", {text = "", texthl = "DiagnosticSignHint", linehl = "", numhl = ""})

---
-- Git colors
local addbg = "#2a342e"    -- dkg12
local changebg = "#443e30" -- dkg8
local deletebg = "#3e2c30" -- dkg2

---
-- Status colors
colorscheme("User1", {guifg = red, guibg = grey15})
colorscheme("User2", {guifg = green, guibg = grey15})
colorscheme("User3", {guifg = yellow, guibg = grey15})
colorscheme("User4", {guifg = blue, guibg = grey15})
colorscheme("User5", {guifg = magenta, guibg = grey15})
colorscheme("User6", {guifg = cyan, guibg = grey15})
colorscheme("User7", {guifg = dark_brown, guibg = grey15})


---
-- Vim editor colors
--
highlight("ColorColumn", {guifg = red, guibg = grey7})
highlight("Conceal", {guifg = grey50})
highlight("Cursor", {guifg = black, guibg = grey78})
-- highlight("lCursor", {})
-- highlight("CursorIM", {})
highlight("CursorColumn", {guibg = grey7})
highlight("CursorLine", {guibg = grey7})
highlight("Directory", {guifg = blue})
highlight("DiffAdd", {guibg = addbg})
highlight("DiffChange", {guibg = grey15})
highlight("DiffDelete", {guifg = deletebg, guibg = deletebg})
highlight("DiffText", {guibg = changebg})
-- highlight("EndOfBuffer", {}) -- links to NonText
highlight("TermCursor", {guifg = black, guibg = grey78})
-- highlight("TermCursorNC", {})
highlight("ErrorMsg", {guifg = red})
highlight("VertSplit", {gui = none, guifg = none, guibg = none})
highlight("Folded", {guifg = cyan, guibg = none})
highlight("FoldColumn", {guifg = cyan, guibg = grey7})
highlight("SignColumn", {guifg = cyan, guibg = grey7})
-- highlight("IncSearch", {gui = none, guifg = black, guibg = yellow})
-- highlight("IncSearch", {gui = none, guifg = white, guibg = defaultbg})
highlight("IncSearch", {gui = bold, guifg = white, guibg = defaultbg})
-- highlight("Substitute", {}) -- links to Search
highlight("LineNr", {guifg = grey30, guibg = grey7})
-- highlight("LineNrAbove", {}) -- links to LineNr
-- highlight("LineNrBelow", {}) -- links to LineNr
highlight("CursorLineNr", {gui = none, guifg = grey78, guibg = grey7})
highlight("MatchParen", {guibg = grey30})
-- highlight("ModeMsg", {})
-- highlight("MsgArea", {})
-- highlight("MsgSeparator", {}) -- links to StatusLine
highlight("MoreMsg", {guifg = blue})
highlight("Nontext", {gui = none, guifg = grey50})
-- highlight("Normal", {guifg = grey78, guibg = black})
-- highlight("Normal", {guifg = defaultfg, guibg = black})
highlight("Normal", {guifg = defaultfg, guibg = defaultbg})
-- highlight("NormalFloat", {}) -- links to Pmenu
-- highlight("NormalNC", {guifg = grey70, guibg = grey15})
highlight("NormalNC", {guifg = grey62, guibg = grey7})
highlight("Pmenu", {guifg = none, guibg = none})
highlight("PmenuSel", {guifg = none, guibg = grey23})
highlight("PmenuSbar", {guibg = grey30})
highlight("PmenuThumb", {guibg = grey39})
highlight("Question", {gui = none, guifg = yellow})
-- highlight("QuickFixLine", {}) -- links to Search
-- highlight("Search", {guifg = black, guibg = yellow})
highlight("Search", {gui = bold, guifg = white, guibg = defaultbg})
highlight("SpecialKey", {guifg = cyan})
highlight("SpellBad", {gui = underline, guibg = errbg})
highlight("SpellCap", {gui = underline, guibg = warnbg})
highlight("SpellLocal", {gui = underline, guibg = errbg})
highlight("SpellRare", {gui = underline, guibg = infobg})
highlight("StatusLine", {gui = none, guifg = grey78, guibg = grey15})
highlight("StatusLineNC", {gui = none, guifg = grey30, guibg = grey15})
highlight("TabLine", {gui = none, guifg = grey50, guibg = grey7})
highlight("TabLineFill", {gui = none})
highlight("TabLineSel", {gui = none, guifg = grey78, guibg = grey23})
highlight("Title", {gui = none, guifg = magenta})
highlight("Visual", {guibg = grey23})
-- highlight("VisualNC", {})
highlight("WarningMsg", {guifg = yellow})
-- highlight("Whitespace", {}) -- Links to NonText
highlight("WildMenu", {guifg = black, guibg = yellow})

---
-- LSP
--
highlight("DiagnosticError", {guifg = errfg, guibg = errbg})
highlight("DiagnosticWarn", {guifg = warnfg, guibg = warnbg})
highlight("DiagnosticInfo", {guifg = infofg, guibg = infobg})
highlight("DiagnosticHint", {guifg = hintfg, guibg = hintbg})

highlight("DiagnosticUnderlineError", {gui = underline})
highlight("DiagnosticUnderlineWarn", {gui = underline})
highlight("DiagnosticUnderlineInfo", {gui = underline})
highlight("DiagnosticUnderlineHint", {gui = underline})

highlight("DiagnosticSignError", {guifg = errfg, guibg = grey7})
highlight("DiagnosticSignWarn", {guifg = warnfg, guibg = grey7})
highlight("DiagnosticSignInfo", {guifg = infofg, guibg = grey7})
highlight("DiagnosticSignHint", {guifg = hintfg, guibg = grey7})

---
-- nvim-cmp
--
highlight("CmpItemAbbr", {guifg = grey78}) -- default link to Pmenu
highlight("CmpItemAbbrDeprecated", {guifg = grey50}) -- default link to Comment
highlight("CmpItemAbbrMatch", {gui = bold, guifg = white}) -- default link to Pmenu
highlight("CmpItemAbbrMatchFuzzy", {gui = bold, guifg = white}) -- default link to Pmenu
highlight("CmpItemKind", {guifg = cyan}) -- default link to Special
highlight("CmpItemMenu", {guifg = grey50}) -- default link to Pmenu

---
-- Telescope
--
-- TelescopeSelection xxx links to Visual
highlight("TelescopeSelectionCaret", {guifg = white, guibg = grey23})
-- TelescopeMultiSelection xxx links to Type
highlight("TelescopeNormal", {guifg = grey78, guibg = black})
-- TelescopePreviewNormal xxx links to Normal
highlight("TelescopeBorder", {guifg = white, guibg = black})
-- TelescopePromptBorder xxx links to TelescopeBorder
-- TelescopeResultsBorder xxx links to TelescopeBorder
-- TelescopePreviewBorder xxx links to TelescopeBorder
-- TelescopeTitle xxx links to TelescopeBorder
highlight("TelescopeTitle", {guifg = grey78, guibg = black})
-- TelescopePromptTitle xxx links to TelescopeTitle
-- TelescopeResultsTitle xxx links to TelescopeTitle
-- TelescopePreviewTitle xxx links to TelescopeTitle
highlight("TelescopeMatching", {gui = bold, guifg = white})
highlight("TelescopePromptPrefix", {guifg = grey50})
-- TelescopePreviewLine xxx links to Visual
-- TelescopePreviewMatch xxx links to Search
-- TelescopePreviewPipe xxx links to Constant
-- TelescopePreviewCharDev xxx links to Constant
-- TelescopePreviewDirectory xxx links to Directory
-- TelescopePreviewBlock xxx links to Constant
-- TelescopePreviewLink xxx links to Special
-- TelescopePreviewSocket xxx links to Statement
-- TelescopePreviewRead xxx links to Constant
-- TelescopePreviewWrite xxx links to Statement
-- TelescopePreviewExecute xxx links to String
-- TelescopePreviewHyphen xxx links to NonText
-- TelescopePreviewSticky xxx links to Keyword
-- TelescopePreviewSize xxx links to String
-- TelescopePreviewUser xxx links to Constant
-- TelescopePreviewGroup xxx links to Constant
-- TelescopePreviewDate xxx links to Directory
-- TelescopePreviewMessage xxx links to TelescopePreviewNormal
-- TelescopePreviewMessageFillchar xxx links to TelescopePreviewMessage
-- TelescopeResultsClass xxx links to Function
-- TelescopeResultsConstant xxx links to Constant
-- TelescopeResultsField xxx links to Function
-- TelescopeResultsFunction xxx links to Function
-- TelescopeResultsMethod xxx links to Method
-- TelescopeResultsOperator xxx links to Operator
-- TelescopeResultsStruct xxx links to Struct
-- TelescopeResultsVariable xxx links to SpecialChar
-- TelescopeResultsLineNr xxx links to LineNr
-- TelescopeResultsIdentifier xxx links to Identifier
-- TelescopeResultsNumber xxx links to Number
-- TelescopeResultsComment xxx links to Comment
-- TelescopeResultsSpecialComment xxx links to SpecialComment
-- TelescopeResultsDiffChange xxx links to DiffChange
-- TelescopeResultsDiffAdd xxx links to DiffAdd
-- TelescopeResultsDiffDelete xxx links to DiffDelete
-- TelescopeResultsDiffUntracked xxx links to NonText

highlight("GitSignsAdd", {guifg = green, guibg = grey7})
highlight("GitSignsChange", {guifg = yellow, guibg = grey7})
highlight("GitSignsDelete", {guifg = red, guibg = grey7})
