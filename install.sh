#!/bin/bash

: ${XDG_CONFIG_HOME:=$HOME/.config}
: ${XDG_DATA_HOME:=$HOME/.local/data}
: ${TERMINFO:=$XDG_DATA_HOME/terminfo}

[[ -d "$XDG_CONFIG_HOME" ]] || mkdir "$XDG_CONFIG_HOME"
[[ -d "$XDG_DATA_HOME" ]] || mkdir -p "$XDG_DATA_HOME"

script_dir=$(cd $(dirname $0); pwd -P)

# Homebrew
bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
brew bundle install --file $script_dir/Brewfile --no-lock

# SSH
SSH_DIR=$HOME/.ssh
[[ -d "$SSH_DIR" ]] || mkdir "$SSH_DIR"
ln -s "$script_dir/ssh/config" "$SSH_DIR/config"

# Git
ln -s "$script_dir/git" "$XDG_CONFIG_HOME/git"

# tmux
ln -s "$script_dir/tmux" "$XDG_CONFIG_HOME/tmux"
infocmp -x tmux-256color > "$script_dir/tmux/tmux-256color.src"
/usr/bin/tic -o "$TERMINFO" -x "$script_dir/tmux/tmux-256color.src"

# Alacritty
ln -s "$script_dir/alacritty" "$XDG_CONFIG_HOME/alacritty"
infocmp -x alacritty > "$script_dir/alacritty/alacritty.src"
/usr/bin/tic -o "$TERMINFO" -x "$script_dir/alacritty/alacritty.src"

# Starship
ln -s "$script_dir/starship" "$XDG_CONFIG_HOME/starship"

# Neovim
NVIM_CONFIG_DIR="$XDG_CONFIG_HOME/nvim"

recompile_lua () {
    local src_dir="$1"
    local dst_dir="$2"

    [[ -d "$src_dir" ]] || return
    [[ -d "$dst_dir" ]] || mkdir -p "$dst_dir"

    fd --extension=lua --search-path="$src_dir" \
        --exec bash -c "[[ {} -nt $dst_dir/{/} ]] && luajit -b {} $dst_dir/{/}"
}

if [[ ! -e "$NVIM_CONFIG_DIR" ]]
then
    ln -s "$script_dir/nvim" "$NVIM_CONFIG_DIR"
fi

nvim --headless -u NONE -c "lua require'bootstrap'.paq()"
luajit -be "$(cat $NVIM_CONFIG_DIR/lua/config/*.lua)" "$NVIM_CONFIG_DIR/init.lua"

for dir in colors ftplugin indent
do
    recompile_lua "$NVIM_CONFIG_DIR/lua/$dir" "$NVIM_CONFIG_DIR/$dir"
done
